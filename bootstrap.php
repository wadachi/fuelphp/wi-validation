<?php
/**
 * Wadachi FuelPHP Validation Package
 *
 * Validation rules and exceptions.
 *
 * @package    wi-validation
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

\Autoloader::add_classes([
  'Wi\\Validation' => __DIR__.'/classes/validation.php',
  'Wi\\ValidationError' => __DIR__.'/classes/validationerror.php',
  'Wi\\Interface_Controller_Validation' => __DIR__.'/classes/interface/controller/validation.php',
  'Wi\\Trait_Controller_Validation' => __DIR__.'/classes/trait/controller/validation.php',
]);

\Lang::load('validation', true);
