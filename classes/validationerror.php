<?php
/**
 * Wadachi FuelPHP Validation Package
 *
 * Validation rules and exceptions.
 *
 * @package    wi-validation
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * 検証例外
 */
class ValidationError extends \Exception
{
  // 検証エラー配列
  protected $validation_errors;

  /**
   * コンストラクタ
   *
   * @access public
   * @param array $validation_errors 検証エラー配列
   */
  public function __construct (array $validation_errors)// <editor-fold defaultstate="collapsed" desc="...">
  {
    parent::__construct('検証エラー');
    $this->validation_errors = $validation_errors;
  }// </editor-fold>

  /**
   * 検証エラー配列を取得する
   *
   * @access public
   */
  final public function getValidationErrors()// <editor-fold defaultstate="collapsed" desc="...">
  {
    return $this->validation_errors;
  }// </editor-fold>
}
