<?php
/**
 * Wadachi FuelPHP Validation Package
 *
 * Validation rules and exceptions.
 *
 * @package    wi-validation
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * コントローラの検証形質
 */
trait Trait_Controller_Validation
{

  private $input_errors = [];

  /**
   * アクション後処理
   * リダイレクトされた時に実行されていないのは
   *
   * @access public
   * @param Fuel\Core\Response $response レスポンス
   * @return Fuel\Core\Response レスポンス
   */
  public function after($response)// <editor-fold defaultstate="collapsed" desc="...">
  {
    if (isset($this->template->content))
    {
      // エラー データ
      $html =
        '<script id="'.self::INPUT_ERRORS_HTML_ID.'" type="application/json">'.
          json_encode($this->input_errors).
        '</script>';

      \View::set_global(self::INPUT_ERRORS_VIEW_VAR, $html, false);
    }

    return parent::after($response);
  }// </editor-fold>

  /**
   * 検証エラーを追加する
   *
   * @access public
   * @param array $input_errors 検証エラー ["field_name" => "message"]
   */
  protected function add_input_errors(array $input_errors)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $this->input_errors = array_merge($this->input_errors, $input_errors);
  }// </editor-fold>
}
