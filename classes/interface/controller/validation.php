<?php
/**
 * Wadachi FuelPHP Validation Package
 *
 * Validation rules and exceptions.
 *
 * @package    wi-validation
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * トースト　コントローラ　インターフェース
 */
interface Interface_Controller_Validation
{
  /**
   * 検証HTML要素ID
   */
  const INPUT_ERRORS_HTML_ID = 'input-errors';

  /**
   * 検証ビュー変数名
   */
  const INPUT_ERRORS_VIEW_VAR = '_input_errors';
}
