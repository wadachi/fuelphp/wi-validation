<?php
/**
 * Wadachi FuelPHP Validation Package
 *
 * Validation rules and exceptions.
 *
 * @package    wi-validation
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * 検証ルール
 */
class Validation extends \Fuel\Core\Validation
{
  /**
   * ISO 8601日時チェック
   *
   * @param string 日時値
   * @return bool 有効な日時場合はtrue、そうでなければfalse
   */
  public function _validation_iso8601_date($val)
  {
    if ($this->_empty($val))
    {
      return true;
    }

    $validator = \Validation::active();
    $pattern = '/^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/';

    return $validator->_validation_match_pattern($val, $pattern);
  }

  /**
   * 全角チェック
   * @param string $val フィールド値
   * @return bool 検証に合格した場合はtrue、そうでなければfalse
   */
  public static function _validation_full_width($val)// <editor-fold defaultstate="collapsed" desc="...">
  {
    return empty($val) || !!preg_match('/^[ぁ-んァ-ヶー一-龠々０-９、。〇〻〆　]+$/u', $val);
  }// </editor-fold>

  /**
   * カナチェック
   * @param string $val フィールド値
   * @return bool 検証に合格した場合はtrue、そうでなければfalse
   */
  public static function _validation_kana($val)// <editor-fold defaultstate="collapsed" desc="...">
  {
    return empty($val) || !!preg_match('/^[ァ-ヶｦ-ﾟー　 ]*$/u', $val);
  }// </editor-fold>

  /**
   * 半角カナチェック
   * @param string $val フィールド値
   * @return bool 検証に合格した場合はtrue、そうでなければfalse
   */
  public static function _validation_half_width_kana($val)// <editor-fold defaultstate="collapsed" desc="...">
  {
    return empty($val) || !!preg_match('/[ｱ-ﾝﾞﾟ]+/u', $val);
  }// </editor-fold>

  /**
   * 漢字が入っていたら偽とする
   * @param string $val フィールド値
   * @return bool 検証に合格した場合はtrue、そうでなければfalse
   */
  public static function _validation_disallow_kanji($val)// <editor-fold defaultstate="collapsed" desc="...">
  {
    return empty($val) || !!preg_match("/([\x{3005}\x{3007}\x{303b}\x{3400}-\x{9FFF}\x{F900}-\x{FAFF}\x{20000}-\x{2FFFF}])(.*|)/u", $val, $matches);
  }// </editor-fold>

  /**
   * 終了日を検証する
   *
   * @param string $val フィールド値（終了日）
   * @param string $start_date_field 開始日フィールド名
   * @return bool 検証に合格した場合はtrue、そうでなければfalse
   */
  public static function _validation_valid_end_date($val, $start_date_field)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $start = \Validation::active()->input($start_date_field);
    $end = $val;

    if (!isset($start) || !isset($end))
    {
      return true;
    }

    try
    {
      $start = \Date::create_from_string($start);
      $end = \Date::create_from_string($end);
    } catch (\Exception $ex) {
      $ex->getMessage();
      return true;
    }

    return $end >= $start;
  }// </editor-fold>

  /**
   * 郵便番号チェック
   * @param string $val フィールド値（郵便番号）
   * @return bool 検証に合格した場合はtrue、そうでなければfalse
   */
  public static function _validation_postal_code($val)// <editor-fold defaultstate="collapsed" desc="...">
  {
    return empty($val) || !!preg_match('/^[\d]{3}[-]{0,1}[\d]{4}+$/', $val);
  }// </editor-fold>

  /**
   * 電話番号チェック
   * @param string $val フィールド値（電話番号）
   * @return bool 検証に合格した場合はtrue、そうでなければfalse
   */
  public static function _validation_phone($val)// <editor-fold defaultstate="collapsed" desc="...">
  {
    return empty($val) || !!preg_match('/^0\d{1,5}-?\d{2,4}-?\d{4}$/',$val,$parts);
  }// </editor-fold>

  /**
   * メールアドレスチェック
   * @param string $val フィールド値（メールアドレス）
   * @return bool 検証に合格した場合はtrue、そうでなければfalse
   */
  public function _validation_valid_email($val)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $mail_regex = '/(?:[^(\040)<>@,;:".\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\[\]\000-\037\x80-\xff])|"[^\\\x80-\xff\n\015"]*(?:\\[^\x80-\xff][^\\\x80-\xff\n\015"]*)*")(?:\.(?:[^(\040)<>@,;:".\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\[\]\000-\037\x80-\xff])|"[^\\\x80-\xff\n\015"]*(?:\\[^\x80-\xff][^\\\x80-\xff\n\015"]*)*"))*@(?:[^(\040)<>@,;:".\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\[\]\000-\037\x80-\xff])|\[(?:[^\\\x80-\xff\n\015\[\]]|\\[^\x80-\xff])*\])(?:\.(?:[^(\040)<>@,;:".\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\[\]\000-\037\x80-\xff])|\[(?:[^\\\x80-\xff\n\015\[\]]|\\[^\x80-\xff])*\]))*/';
    return empty($val) || !!preg_match($mail_regex,$val);
  }// </editor-fold>

  /**
   * 特定の他の送信されたフィールドの文字列値と一致しない
   * （両方の文字列である必要があり、チェックが敏感なタイプだ）
   *
   * @param string $val フィールド値
   * @param string $field 比較されたフィールドの名前
   * @return bool 検証に合格した場合はtrue、そうでなければValidation_Errorが発生す
   */
  public static function _validation_not_match_field($val, $field)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $validator = \Validation::active();

    if (trim($validator->input($field)) === trim($val))
    {
      $validating = $validator->active_field();
      throw new Validation_Error($validating, $val, ['not_match_field' => [$field]], [$validator->field($field)->label, $validating->label]);
    }

    return true;
  }// </editor-fold>

  /**
   * 比較入力にマッチする値と、
   * 条件付きでは、別のフィールドの完了に基づいて、現在のフィールドを完了する必要がある
   *
   * @param string $val フィールド値
   * @param string $field 比較されたフィールドの名前
   * @param string $field 比較されたフィールドの一致した値
   * @return bool 検証に合格した場合はtrue、そうでなければValidation_Errorかfalseが発生す
   */
  public static function _validation_required_with_when($val, $field, $compare, $strict = false)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $validator = \Validation::active();
    $field_val = $validator->input($field);

    if ($validator->_validation_match_value($field_val, $compare, $strict))
    {
      return $validator->_validation_required_with($val, $field);
    }

    return true;
  }// </editor-fold>

  /**
   * セミコロンで区切られた文字列の値リストにマッチ
   *
   * @param string $val フィールド値
   * @param string $collection セミコロンで区切られた文字列の値リスト
   * @return bool 検証に合格した場合はtrue、そうでなければfalse
   */
  public static function _validation_match_one($val, $collection)// <editor-fold defaultstate="collapsed" desc="...">
  {
    if (empty($val))
    {
      return true;
    }

    $validator = \Validation::active();
    return $validator->_validation_match_collection($val, explode(';', $collection));
  }// </editor-fold>
}
