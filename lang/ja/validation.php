<?php
/**
 * Wadachi FuelPHP Validation Package
 *
 * Validation rules and exceptions.
 *
 * @package    wi-validation
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * 検証ルールのメッセージ文字列
 * 言語：ja-JP
 */
return [
  'full_width'             => '全角文字で入力してください',
  'kana'                   => 'カタカナで入力してください',
  'half_width_kana'        => '半角カタカナで入力してください',
  'disallow_kanji'         => ':labelを確認してください',
  'valid_end_date'         => '日付範囲が不正です',
  'postal_code'            => '半角数値7桁で入力してください',
  'phone'                  => '入力された番号が不正です',
  'valid_email'            => 'メールアドレスが不正です',
  'not_match_field'        => '『:param:1』と『:param:2』が同じ入力内容です',
  'match_one'              => '『:param:1』と一致していません',
  'iso8601_date'           => '日付が不正です',
];
