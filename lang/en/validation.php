<?php
/**
 * Wadachi FuelPHP Validation Package
 *
 * Validation rules and exceptions.
 *
 * @package    wi-validation
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * 検証ルールのメッセージ文字列
 * 言語：en-US
 */
return [
  'full_width'             => 'Full-width characters only',
  'kana'                   => 'Full-width kana only',
  'half_width_kana'        => 'Half-width kana only',
  'disallow_kanji'         => 'Kanji not allowed',
  'valid_end_date'         => 'Invalid date range',
  'postal_code'            => 'Invalid postal code',
  'phone'                  => 'Invalid phone number',
  'valid_email'            => 'Invalid email address',
  'not_match_field'        => '『:param:1』 and 『:param:2』 must be different',
  'match_one'              => '『:param:1』 is not an option',
  'iso8601_date'           => 'The field :label must contain a valid formatted date',
];
